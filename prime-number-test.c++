#include <iostream>


int main(int argc, char* argv[])
{
	int number = 73; //why 73?
	
	
	//check whether number is a prime
	bool isPrime = true; //assume the number was a prime
	
	for(int i = 2; i < number; ++i)
	{
		if(number % i  ==  0)
		{
			//found a divisor, thus number cannot be prime
			isPrime = false;
			break;
		}
	}
	
	//print results
	if(isPrime)
	{
		std::cout << number << " is a prime number." << std::endl;
	}
	else
	{
		std::cout << number << " is not a prime number." << std::endl;
	}
}
